use actix_web::{middleware::Logger, post, web, App, HttpResponse, HttpServer, Responder};
use anyhow::{Context, Result};
use log::{debug, trace, warn};
use regex::Regex;
use std::path::PathBuf;
use std::{net::SocketAddr, time::Duration};
use structopt::StructOpt;
use url::Url;

#[derive(StructOpt, Debug)]
#[structopt(
    name = "mail-credential-syncer",
    author,
    about,
    global_settings = &[structopt::clap::AppSettings::ColoredHelp],
)]
struct CliConfig {
    /// Local socket address to listen on
    #[structopt(long)]
    listen: SocketAddr,
    // TODO Implement all of the below!
    // /// Username for basic auth of HTTP endpoint
    // #[structopt(long, env)]
    // basic_auth_username: String,
    //
    // /// Password for basic auth of HTTP endpoint
    // #[structopt(long, env)]
    // basic_auth_password: String,
    //
    // /// Path to file containing Keycloak UUID to mail address mappings
    // #[structopt(long)]
    // mailmap: PathBuf,
    //
    // /// Path to a post-sync hook executable
    // #[structopt(long)]
    // post_sync_hook: PathBuf,
    //
    // /// Keycloak URL for incoming webhook validation and periodic syncs
    // #[structopt(long)]
    // keycloak_url: Url,
    //
    // /// Keycloak admin username
    // #[structopt(long, env)]
    // keycloak_admin_username: String,
    //
    // /// Keycloak admin password
    // #[structopt(long, env)]
    // keycloak_admin_password: String,
}

#[post("/webhook")]
async fn webhook(data: web::Json<serde_json::Value>) -> impl Responder {
    // We're getting a `serde_json::Value` here instead of trying to deserialize into a struct
    // because we're only interested in a small subset of the data and we don't care about
    // validation errors in case the struct doesn't match the data sent. I think this gives us more
    // flexibility in handling and ignoring most validation errors that we see while still ensuring
    // we get valid JSON to start with.

    trace!("Received\n{:#}", data);

    if let Some(hash) = data.pointer("/representation/attributes/mail_password_hash/0") {
        debug!("Received update with mail_password_hash: {:#}", hash);

        if let Some(hash_value) = hash.as_str() {
            let re = Regex::new(r"^\$2y\$(.*)$").unwrap();
            if let Some(hash_last_part) = re
                .captures(hash_value)
                .and_then(|captured_value| captured_value.get(1))
            {
                let final_hash_value = hash_last_part.as_str();
                // TODO Actually do something interesting here with the value.
            } else {
                warn!("Received hash but it has wrong format");
                debug!("Hash in question is {:#}", hash_value);
                return HttpResponse::BadRequest().body("Hash has wrong format");
            }
        } else {
            warn!("Received update with mail_password_hash but ");
            return HttpResponse::BadRequest().body("Empty hash value");
        }
    }

    HttpResponse::Ok().body("Hello world!")
}

#[actix_web::main]
async fn main() -> Result<()> {
    std::env::set_var("RUST_LOG", "mail_credential_syncer=debug,actix_web=info");
    pretty_env_logger::init();

    let args = CliConfig::from_args();

    actix_rt::spawn(async move {
        let mut interval = actix_rt::time::interval(Duration::from_secs(10));
        loop {
            interval.tick().await;
            // TODO Do some periodic syncing somehow
            debug!("syncing");
        }
    });

    HttpServer::new(|| App::new().wrap(Logger::default()).service(webhook))
        .bind(args.listen)?
        .run()
        .await
        .context("Failed to run actix")
}
