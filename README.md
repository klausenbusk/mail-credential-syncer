# Mail Credential Syncer

Small tool to sync mail passwords from Keycloak to Dovecot

## How to run

First of all, we need to start a pre-configured Keycloak server to run our tests against:

    git clone https://github.com/svenstaro/keycloak-http-webhook-provider.git
    cd keycloak-http-webhook-provider
    mvn clean install
    keycloak/run-keycloak-container.sh

Once Keycloak is running, we can start our webhook server:

    cargo run -- --listen 0.0.0.0:5000

Now go to your Keycloak server at http://localhost:8080, login via admin/admin and add a new user
attribute called `mail_password_hash` to a user of your choice. The webhook server should then give
you some output of what's happening.
